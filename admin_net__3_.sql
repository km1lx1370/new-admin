-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 12-06-2022 a las 00:09:07
-- Versión del servidor: 5.7.36
-- Versión de PHP: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `admin.net`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignacionempleados`
--

DROP TABLE IF EXISTS `asignacionempleados`;
CREATE TABLE IF NOT EXISTS `asignacionempleados` (
  `idAsignacionEmpleados` int(11) NOT NULL,
  `Ubicacion` varchar(45) NOT NULL,
  PRIMARY KEY (`idAsignacionEmpleados`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `asignacionempleados`
--

INSERT INTO `asignacionempleados` (`idAsignacionEmpleados`, `Ubicacion`) VALUES
(1, 'SedeA    '),
(2, 'sedeB      '),
(3, 'SedeC    '),
(4, 'Colegio   ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargo`
--

DROP TABLE IF EXISTS `cargo`;
CREATE TABLE IF NOT EXISTS `cargo` (
  `idCargo` int(11) NOT NULL,
  `nombreCargo` varchar(45) NOT NULL,
  PRIMARY KEY (`idCargo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cargo`
--

INSERT INTO `cargo` (`idCargo`, `nombreCargo`) VALUES
(1, 'Administradores'),
(2, 'Aseadores'),
(3, 'Guardas'),
(4, 'Rector'),
(6, 'Aseador jefe');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE IF NOT EXISTS `clientes` (
  `idClientes` int(11) NOT NULL,
  `Nombre` varchar(20) NOT NULL,
  `Apellido` varchar(20) NOT NULL,
  `Cedula` int(11) NOT NULL,
  `Telefono` int(11) NOT NULL,
  `Email` varchar(45) NOT NULL,
  `Ubicacion` varchar(45) NOT NULL,
  `Contraseña` varchar(45) NOT NULL,
  `codigoDocumento` int(4) NOT NULL,
  PRIMARY KEY (`idClientes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contratoservicio`
--

DROP TABLE IF EXISTS `contratoservicio`;
CREATE TABLE IF NOT EXISTS `contratoservicio` (
  `Contrato` int(11) NOT NULL,
  `Clientes_idClientes` int(11) NOT NULL,
  `AsignacionEmpleados_idAsignacionEmpleados` int(11) NOT NULL,
  PRIMARY KEY (`Clientes_idClientes`,`AsignacionEmpleados_idAsignacionEmpleados`),
  KEY `fk_ContratoServicio_AsignacionEmpleados1_idx` (`AsignacionEmpleados_idAsignacionEmpleados`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

DROP TABLE IF EXISTS `empleados`;
CREATE TABLE IF NOT EXISTS `empleados` (
  `numeroDocumento` int(11) NOT NULL,
  `idCargo` int(11) NOT NULL,
  `nombreEmpleado` varchar(20) NOT NULL,
  `apellidoEmpleado` varchar(20) NOT NULL,
  `telefonoEmpleado` int(11) NOT NULL,
  `emailEmpleado` varchar(45) NOT NULL,
  `idAsignacionEmpleados` int(11) NOT NULL,
  PRIMARY KEY (`numeroDocumento`) USING BTREE,
  KEY `Cargo_idCargo_2` (`numeroDocumento`,`idCargo`) USING BTREE,
  KEY `fk_Empleados_AsignacionEmpleados1_idx` (`idCargo`) USING BTREE,
  KEY `asignacionempleados` (`idAsignacionEmpleados`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

DROP TABLE IF EXISTS `servicios`;
CREATE TABLE IF NOT EXISTS `servicios` (
  `ServicioAseo` int(11) NOT NULL,
  `ServicioSeguridad` int(11) NOT NULL,
  `ServiciosAseoSeguridad` int(11) NOT NULL,
  `ContratoServicio_Clientes_idClientes` int(11) NOT NULL,
  PRIMARY KEY (`ContratoServicio_Clientes_idClientes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `contratoservicio`
--
ALTER TABLE `contratoservicio`
  ADD CONSTRAINT `fk_ContratoServicio_AsignacionEmpleados1` FOREIGN KEY (`AsignacionEmpleados_idAsignacionEmpleados`) REFERENCES `asignacionempleados` (`idAsignacionEmpleados`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_ContratoServicio_Clientes1` FOREIGN KEY (`Clientes_idClientes`) REFERENCES `clientes` (`idClientes`) ON DELETE CASCADE;

--
-- Filtros para la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD CONSTRAINT `asignacionempleados` FOREIGN KEY (`idAsignacionEmpleados`) REFERENCES `asignacionempleados` (`idAsignacionEmpleados`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_Empleados_AsignacionEmpleados1` FOREIGN KEY (`idCargo`) REFERENCES `cargo` (`idCargo`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_Empleados_Cargo` FOREIGN KEY (`numeroDocumento`) REFERENCES `empleados` (`numeroDocumento`) ON DELETE CASCADE;

--
-- Filtros para la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD CONSTRAINT `fk_Servicios_ContratoServicio1` FOREIGN KEY (`ContratoServicio_Clientes_idClientes`) REFERENCES `contratoservicio` (`Clientes_idClientes`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
